﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : GameUnit {

	public GameObject nextCheckPoint;
	public float speed;
	public GameObject target = null;
	public Weapon weapon;
	public float distanceMinReperage;
	public float distanceMaxReperage;
	public float angleVision;
	private GameObject player;
	private GameObject[] pathList;
	private GameObject leg;
	public List<Vector3> paths;
	public bool triggerWin = false;
	public GameObject test;

	void Start () {
		weapon = GameObject.Instantiate (weapon, this.transform);
		weapon.gameObject.layer = 10;
		weapon.equip ();
		weapon.setIsBot ();
		this.player = GameObject.FindGameObjectsWithTag ("Player") [0];
		pathList = GameObject.FindGameObjectsWithTag ("path");

		foreach (Transform child in transform) {
			if (child.name == "leg")
				leg = child.gameObject;
		}
		leg.GetComponent<Animator> ().SetBool ("walk", false);
	}

	public override void death()
	{
		if (triggerWin)
			Camera.main.GetComponent<CameraController> ().s.playerWin ();
		base.death ();
	}
	
	protected void Update () {
/*			if (Input.GetKey (KeyCode.P))
				this.findPlayer ();
*/

		base.Update ();
		if (!this.canDestoy) {
			if (target == null) {
				//Suis les Checkpoint
				if (nextCheckPoint != null) {
					leg.GetComponent<Animator> ().SetBool ("walk", true);
					transform.position = (Vector3.MoveTowards (transform.position, nextCheckPoint.transform.position, speed));
					Vector2 direction = (nextCheckPoint.gameObject.transform.position - transform.position).normalized;
					transform.up = direction * -1;
				}
			} else {
				//Une cible il tire
				transform.position = (Vector3.MoveTowards (transform.position, target.transform.position, speed));
				Vector2 direction = (target.transform.position - transform.position).normalized;
				transform.up = direction * -1;
				weapon.shoot ();
			}

			if (target == null && player != null) {
				if (Vector2.Distance (this.transform.position, player.transform.position) <= this.distanceMinReperage)
					target = player;
				else if (Vector2.Distance (this.transform.position, player.transform.position) <= this.distanceMaxReperage) {
					Vector3 distance = player.transform.position - transform.position;
					float isForward = Vector3.Dot (distance, -transform.up);
					if (Vector3.Angle (-transform.up, distance) < this.angleVision && isForward > 0) {
						target = this.raycastTag ("Player", this.transform.position, player.transform.position);
					}
				}
			}
		}
	}

	private void findPlayer()
	{
		List<GameObject> path = calculPath (this.transform.position, this.player, new List<GameObject> (), 20);
		affPath (this.transform.position, path);
	}

	private void affPath(Vector3 start, List<GameObject> path)
	{
		Debug.Log ("aff" + path.Count);
		foreach (GameObject point in path) {
			Debug.DrawRay (start, point.transform.position - start, new Color (0, 1, 1));
			start = point.transform.position;
		}

	}

	private List<GameObject> calculPath(Vector3 start, GameObject cible, List<GameObject> doorTake, int stop)
	{
		stop--;
		List<GameObject> ret = null;
		if (stop != 0) {
			Debug.Log ("test");
			List<List<GameObject>> allPath = new List<List<GameObject>> ();
			GameObject obj = this.raycastTag (cible.tag, start, cible.transform.position);
			if (obj != null) {
				ret = new List<GameObject> ();
				ret.Add (obj);
			} else {
				foreach (GameObject objLoop in this.pathList) {
					if (!doorTake.Contains (objLoop)) {
						Debug.DrawRay (start, cible.transform.position - start, new Color (255, stop * 20, 255));
						obj = this.raycastTag (objLoop.tag, start, objLoop.transform.position);
						if (obj != null) {
							List<GameObject> doorTakeCpy = this.cloneList (doorTake);
							doorTakeCpy.Add (obj);
							List<GameObject> calc = new List<GameObject> ();
							calc.Add (obj);
							calc.AddRange (calculPath (obj.transform.position, cible, doorTakeCpy, stop));
							allPath.Add (calc);
						}
					}
				}
			}
			foreach (List<GameObject> g in allPath) {
				if (ret == null || ret.Count > g.Count)
					ret = g;
			}
		}
		return (ret == null)?new List<GameObject>():ret;
	}

	private List<GameObject> cloneList(List<GameObject> original)
	{
		List<GameObject> ret = new List<GameObject> ();
		foreach (GameObject obj in original) {
			ret.Add (obj);
		}
		return ret;
	}

	public GameObject raycastTag(string tag, Vector3 depart, Vector3 cible)
	{
		RaycastHit2D[] hit = Physics2D.RaycastAll (this.transform.position, cible - depart);

		if (hit != null) {
			foreach (RaycastHit2D h in hit) {
				if (h.collider.gameObject.tag != "Enemy" && h.collider.gameObject.tag != "weapon" && h.collider.gameObject.tag != "Door" && h.collider.tag != "ignorePath") {
					//if (cible == test.transform.position) {
//						Debug.Log (h.collider.name);
					//}
						if (h.transform.tag == tag) {
						return h.collider.gameObject;
					}
						else {
						
		//				print ("BREAK:" + h.collider.name);
						break;
					}
				}
			}
		}
		return null;
	}
}
