﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class EndgameUI : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void quit(){
		SceneManager.LoadScene ("menu");
	}


	public void replay(){
		SceneManager.LoadScene (SceneManager.GetActiveScene().name);
	}
}
