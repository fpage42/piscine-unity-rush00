﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

	public Sprite attach;
	public Sprite map;
	public GameObject bullet;
	public float speed = 1;
	public AudioClip sound;
	public float dispersion;
	public int amo = 100;
	private bool isBot = false;
	public string weaponname;

	public float time;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void setIsBot()
	{
		this.isBot = true;
	}

	public void equip(){
		print ("Equip");
		this.transform.localPosition = new Vector3 (0.189f, -0.31f, 0f);
		this.transform.rotation = new Quaternion (0f, 0f,0f, 0f);
		this.GetComponent<SpriteRenderer> ().sprite = this.attach;
	}

	public void drop(Transform player){
		print ("Drop");
		this.GetComponent<SpriteRenderer> ().sprite = this.map;
		this.transform.position = player.position;
		this.transform.Translate (player.up * -2, Space.World);
	}
	public void shoot(){
		GameObject bullet;
		if (time < Time.fixedTime - 1f / speed) {
			time = Time.fixedTime;
			if (amo > 0 || this.isBot || amo <= -1) {
				amo--;
				this.GetComponent<AudioSource> ().PlayOneShot (this.sound, 1f);
				bullet = GameObject.Instantiate (this.bullet, this.transform.position - (this.transform.up * 0.5f), Quaternion.identity);
				bullet.gameObject.layer = 10;
				bullet.GetComponent<Bullet> ().setForce (dispersionRandom ());
				bullet.transform.rotation = transform.rotation * Quaternion.Euler (0, 0, 90);
				bullet.GetComponent<Bullet> ().setLayer (this.transform.parent.gameObject.layer);
			}
		}
	}
		
	private Vector2 dispersionRandom() {
		Vector2 random;
		random = this.transform.up * -0.5f;
		random = new Vector2 (random.x + Random.Range (-this.dispersion, this.dispersion), random.y + Random.Range (-dispersion, dispersion));
		return random;
	}
}
