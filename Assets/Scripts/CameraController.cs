﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CameraController : MonoBehaviour {

	public GameObject player;
	public Text hud;
	public Texture2D cursor;
	public GameObject EndgameUI;
	private bool lose = false;
	public soundManager s;
	// Use this for initialization
	void Start () {
		transform.Rotate(new Vector3 (0f,0f,0.2f));
		Cursor.SetCursor (cursor, new Vector2(0,0), UnityEngine.CursorMode.Auto);
	}
	
	// Update is called once per frame
	void Update () {
		if (player != null) {
			transform.position = new Vector3 (player.transform.position.x, player.transform.position.y, transform.position.z);

			if (player.gameObject.GetComponent<PlayerController> ().weapon != null) {
				if (player.gameObject.GetComponent<PlayerController> ().weapon.GetComponent<Weapon> ().amo >= 0)
					hud.text = player.gameObject.GetComponent<PlayerController> ().weapon.GetComponent<Weapon> ().weaponname + "\n" + player.gameObject.GetComponent<PlayerController> ().weapon.GetComponent<Weapon> ().amo;
				else
					hud.text = player.gameObject.GetComponent<PlayerController> ().weapon.GetComponent<Weapon> ().weaponname + "\n INF";
			}
		}
	}
}
