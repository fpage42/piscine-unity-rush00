﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class soundManager : MonoBehaviour {

	public AudioClip lose, win;
	public  bool menu = false;
	void Update()
	{
		if (menu && !this.GetComponent<AudioSource>().isPlaying)
			Camera.main.GetComponent<CameraController> ().EndgameUI.SetActive (true);
	}

	public void playerLose()
	{
		Debug.Log ("call");
		this.gameObject.GetComponent<AudioSource> ().PlayOneShot (lose);
		menu = true;
		Text[] textes= Camera.main.GetComponent<CameraController> ().EndgameUI.GetComponentsInChildren<Text> ();
		foreach (Text t in textes) {
			if (t.name == "title")
				t.text = "You Lose !";
		}
	}
	public void playerWin()
	{
		Debug.Log ("call win");
		this.GetComponent<AudioSource> ().PlayOneShot (this.win);
		menu = true;
		Text[] textes= Camera.main.GetComponent<CameraController> ().EndgameUI.GetComponentsInChildren<Text> ();
		foreach (Text t in textes) {
			if (t.name == "title")
				t.text = "You Win !";
		}
	}
}
