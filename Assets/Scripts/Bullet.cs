﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public Vector2 dir;
	public float ttl;
	private Rigidbody2D rb;
	private int layer;

	// Use this for initialization
	void Start () {
		Invoke("timeout", ttl);
	}

	public void setForce(Vector2 dir)
	{	
		rb = this.GetComponent<Rigidbody2D> ();
		rb.AddForce (dir * 5000);
	}

	public void setLayer(int layer)
	{
		this.gameObject.layer = layer;
	}

	// Update is called once per frame
	void Update () {
		//transform.Translate (dir, Space.World);

	}

	void timeout() {
		GameObject.DestroyObject (this.gameObject);
	}

	void OnCollisionEnter2D(Collision2D collider) {
		GameUnit unlucky;
		unlucky = collider.gameObject.GetComponent<GameUnit> ();

		if (unlucky) {
			unlucky.death();
		}
		Destroy (this.gameObject);
	}
}
