﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUnit : MonoBehaviour {

	public int health;
	public List<AudioClip> deathSounds = new List<AudioClip> ();
	protected bool canDestoy = false;

	// Use this for initialization
	void Start () {			
	}
	
	// Update is called once per frame
	protected void Update () {
		if (canDestoy && !this.gameObject.GetComponent<AudioSource> ().isPlaying)
			Destroy (this.gameObject);
	}

	virtual public void death(){
		this.gameObject.GetComponent<AudioSource> ().PlayOneShot (deathSounds [Random.Range (0, deathSounds.Count-1)]);
		this.canDestoy = true;
	}
}
