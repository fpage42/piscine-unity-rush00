﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour {

	public GameObject next;

	void Start () {
		
	}
	
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D collider){
		if (!collider.isTrigger && collider.tag == "Enemy") {
			collider.gameObject.GetComponent<Enemy> ().nextCheckPoint = this.next;
		}
	}
}
