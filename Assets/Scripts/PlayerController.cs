﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController: GameUnit {
	
	public float speed = 5;


	private List<GameObject> CanTakeWeapons = new List<GameObject>(); 
	private GameObject leg;
	public GameObject weapon = null;
	public bool freeze = false;
	public AudioClip takeWeapon;

	void Start () {
		foreach (Transform child in transform) {
			if (child.name == "leg")
				leg = child.gameObject;
		}
		leg.GetComponent<Animator> ().SetBool ("walk", false);
	}

	protected void Update () {
		if (!freeze) {

			if (Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.D)) {
				leg.GetComponent<Animator> ().SetBool ("walk", true);
				if (Input.GetKey (KeyCode.W)) {
					this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0f, 10f * speed));
				}
				if (Input.GetKey (KeyCode.S)) {
					this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0f, -10f * speed));
				}
				if (Input.GetKey (KeyCode.A)) {
					this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (-10f * speed, 0));
				}
				if (Input.GetKey (KeyCode.D)) {
					this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (10f * speed, 0));
				}

			} else {
				leg.GetComponent<Animator> ().SetBool ("walk", false);
			}

			Vector2 mouseScreenPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			Vector2 direction = (mouseScreenPosition - (Vector2)transform.position).normalized;
			transform.up = direction * -1;

			if (Input.GetKey (KeyCode.E)) {
				equipWeapon ();
			}
			if (Input.GetKey (KeyCode.V)) {
				print (CanTakeWeapons.Count);
			}

			if (Input.GetKey (KeyCode.Mouse0) || Input.GetKey(KeyCode.Space))
				shoot ();

			if (Input.GetKey (KeyCode.Q) || Input.GetKeyDown(KeyCode.Mouse1)) {
				dropWeapon ();
			}
		}
	}

	void OnTriggerEnter2D(Collider2D collider){
		if (collider.gameObject.tag == "weapon")
			CanTakeWeapons.Add (collider.gameObject);
	}

	void OnTriggerExit2D(Collider2D collider){
		CanTakeWeapons.Remove (collider.gameObject);
	}

	void equipWeapon()
	{
		if (this.CanTakeWeapons.Count > 0 && this.weapon == null) {
			this.weapon = CanTakeWeapons [0];
			this.CanTakeWeapons.RemoveAt(0);
			this.weapon.gameObject.transform.SetParent(this.transform);
			this.weapon.GetComponent<Weapon> ().equip ();
			this.gameObject.GetComponent<AudioSource> ().PlayOneShot (takeWeapon);
		}
	}

	void dropWeapon()
	{
		if (this.weapon) {
			this.weapon.gameObject.transform.SetParent(null);
			this.weapon.GetComponent<Weapon>().drop (this.transform);
			this.weapon = null;
		}
	}

	void shoot(){
		if (weapon) {
			weapon.GetComponent<Weapon>().shoot ();
		}
	}

	public override void death(){
		Camera.main.GetComponent<CameraController> ().s.playerLose ();
		Destroy (this.gameObject);
	}

}
